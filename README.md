# ECS ECR Container Scan Kickstarter

Terraform module to create an ECS ECR Container Scan Kickstarter task.

This task:

- fetches repositories in the default registry for the region it's deployed in
- fetches images in found repositories
- kicks off an ECR scan
- publishes some finding metadata about critical and high findings to an SNS topic

## Requirements

- ECS cluster
- VPC & subnet with connectivity to ECR

## Outputs

An SNS topic for basic alerting.
