terraform {
  backend "remote" {
    organization = "open-source-devex"
    workspaces {
      name = "terraform-modules-aws-ecs-inspector-kickstarter-simple"
    }
  }
}

provider "aws" {
  region = "eu-west-1"
}

module "container-scan-kickstarter" {
  enable = true

  source = "../../"

  aws_region = "eu-west-1"
  name       = "test-container-scan-kickstarter"

  ecs_subnet_ids = module.vpc.public_subnets

  vpc_id         = module.vpc.vpc_id
  ecs_cluster_id = module.ecs.this_ecs_cluster_id

  cloudwatch_cron_schedule = "cron(0 3 * * ? *)"
}



module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "test-container-scan-kickstarter"

  cidr = "10.0.0.0/16"

  azs            = ["eu-west-1a"]
  public_subnets = ["10.0.100.0/24"]
}

module "ecs" {
  source  = "terraform-aws-modules/ecs/aws"
  version = "v2.0.0"

  name = "test-container-scan-kickstarter"
}
