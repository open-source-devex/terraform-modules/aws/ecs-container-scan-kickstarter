locals {
  create_sns_topic = var.enable && var.create_sns_topic

  sns_topic_arn = local.create_sns_topic ? join("", aws_sns_topic.sns_topic_ecr_container_scan_alerts.*.arn) : var.sns_topic_arn
}

resource "aws_cloudwatch_event_target" "cloudwatch_event_ecr_container_scan_target" {
  count = var.enable ? 1 : 0

  target_id = "ecr_container_scan"
  rule      = aws_cloudwatch_event_rule.cloudwatch_event_ecr_container_scan_kickstart[0].name
  arn       = var.ecs_cluster_id
  role_arn  = aws_iam_role.ecs_container_kickstarter_events[0].arn

  ecs_target {
    launch_type         = var.ecs_launch_type
    task_count          = 1
    task_definition_arn = aws_ecs_task_definition.ecs_ecr_container_scan_kickstarter[0].arn

    network_configuration {
      security_groups  = [aws_security_group.ecr_container_scan_kickstarter_allow_egress[0].id]
      subnets          = var.ecs_subnet_ids
      assign_public_ip = true
    }
  }
}

resource "aws_sns_topic" "sns_topic_ecr_container_scan_alerts" {
  count = local.create_sns_topic ? 1 : 0

  name = "${var.name}-ecr-container-scan-alerts-topic"

  kms_master_key_id = "alias/aws/sns"
}

resource "aws_cloudwatch_event_rule" "cloudwatch_event_ecr_container_scan_kickstart" {
  count = var.enable ? 1 : 0

  name        = "${var.name}-container-scan-kickstart"
  description = "Kickstarts an ECR container scan"

  schedule_expression = var.cloudwatch_cron_schedule != null ? var.cloudwatch_cron_schedule : "cron(0 3 * * ? 2000)"
}

resource "aws_ecs_task_definition" "ecs_ecr_container_scan_kickstarter" {
  count = var.enable ? 1 : 0

  family                   = "${var.name}-ecr-container-scan-kickstarter"
  network_mode             = "awsvpc"
  cpu                      = 256
  memory                   = 512
  requires_compatibilities = ["FARGATE"]
  task_role_arn            = aws_iam_role.ecr_container_scan_kickstarter_role[0].arn
  execution_role_arn       = aws_iam_role.ecr_container_scan_kickstarter_role[0].arn

  tags = var.tags

  container_definitions = <<DEFINITION
[
  {
    "cpu": ${var.ecr_container_scan_kickstarter_container_task_cpu},
    "environment": [
        {
            "name": "SNS_ALERT_TOPIC_ARN",
            "value": "${local.sns_topic_arn}"
        },
        {
            "name": "AWS_DEFAULT_REGION",
            "value": "${var.aws_region}"
        }
    ],
    "essential": true,
    "image": "${var.ecr_container_scan_kickstarter_container_image}",
    "memory": ${var.ecr_container_scan_kickstarter_container_task_memory},
    "memoryReservation": ${var.ecr_container_scan_kickstarter_container_memory_reservation},
    "name": "${var.name}-ecr-container-scan-kickstarter",
    "logConfiguration": {
        "logDriver": "awslogs",
        "options": {
            "awslogs-create-group": "true",
            "awslogs-group": "${var.cloudwatch_log_group}",
            "awslogs-region": "${var.aws_region}",
            "awslogs-stream-prefix": "ecs"
        }
    }
  }
]
DEFINITION
}

# Add security group that does not allow inbound traffic
resource "aws_security_group" "ecr_container_scan_kickstarter_allow_egress" {
  count = var.enable ? 1 : 0

  name        = "${var.name}_group_allow_egress"
  description = "Allow outbound traffic"
  vpc_id      = var.vpc_id

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  tags = var.tags
}


resource "aws_iam_policy" "container_scan_kickstarter_policy" {
  count = var.enable ? 1 : 0

  name        = "${var.name}_container_scan_kickstarter_policy"
  path        = "/"
  description = "Policy for handling automatic AWS ECR scanning"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "AllowLogging",
      "Effect": "Allow",
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents",
        "logs:DescribeLogStreams"
      ],
      "Resource": ["arn:aws:logs:*:*:*"]
    },
    {
      "Sid": "AllowSNSTopicPublishing",
      "Action": ["sns:Publish"],
      "Effect": "Allow",
      "Resource": "${local.sns_topic_arn}"
    },
    {
      "Sid": "AllowECRScanning",
      "Action": [
        "ecr:DescribeRepositories",
        "ecr:DescribeImages",
        "ecr:StartImageScan",
        "ecr:DescribeImageScanFindings"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_role" "ecr_container_scan_kickstarter_role" {
  count = var.enable ? 1 : 0

  name = "${var.name}_container_scan_role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": [
          "ec2.amazonaws.com",
          "ecs-tasks.amazonaws.com"
        ]
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "ecr_container_scan_kickstarter_role_policy_attachment" {
  count = var.enable ? 1 : 0

  role       = aws_iam_role.ecr_container_scan_kickstarter_role[0].name
  policy_arn = aws_iam_policy.container_scan_kickstarter_policy[0].arn
}

resource "aws_iam_role" "ecs_container_kickstarter_events" {
  count = var.enable ? 1 : 0
  name  = "${var.name}_container_scan_events"

  assume_role_policy = <<DOC
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "events.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
DOC
}

resource "aws_iam_role_policy" "ecs_events_container_scan_run_task" {
  count = var.enable ? 1 : 0

  name = "${var.name}_container_scan_task_policy"
  role = aws_iam_role.ecs_container_kickstarter_events[0].id

  policy = <<DOC
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
              "ecs:RunTask",
              "ecs:StartTask"
            ],
            "Resource": "${aws_ecs_task_definition.ecs_ecr_container_scan_kickstarter[0].arn}"
        },
        {
            "Effect": "Allow",
            "Action": "iam:PassRole",
            "Resource": [
                "${aws_iam_role.ecr_container_scan_kickstarter_role[0].arn}"
            ],
            "Condition": {
                "StringLike": {
                    "iam:PassedToService": "ecs-tasks.amazonaws.com"
                }
            }
        }
    ]
}
DOC
}
