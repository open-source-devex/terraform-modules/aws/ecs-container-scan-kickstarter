output "sns_topic_ecr_container_scan_alerts" {
  value = local.sns_topic_arn
}

output "cloudwatch_log_group_ecr_container_scan_kickstarter" {
  value = var.cloudwatch_log_group
}

output "ecr_container_scan_kickstarter_role_arn" {
  value = join(" ", aws_iam_role.ecr_container_scan_kickstarter_role.*.arn)
}

output "ecr_container_scan_kickstarter_role_id" {
  value = join(" ", aws_iam_role.ecr_container_scan_kickstarter_role.*.id)
}
